-- MySQL dump 10.13  Distrib 5.6.11, for Win32 (x86)
--
-- Host: localhost    Database: takahashi_osamu
-- ------------------------------------------------------
-- Server version	5.6.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `branches`
--

DROP TABLE IF EXISTS `branches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `branches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `branches`
--

LOCK TABLES `branches` WRITE;
/*!40000 ALTER TABLE `branches` DISABLE KEYS */;
INSERT INTO `branches` VALUES (1,'本店'),(2,'中野店'),(3,'札幌店'),(4,'名古屋店');
/*!40000 ALTER TABLE `branches` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` varchar(500) DEFAULT NULL,
  `message_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
INSERT INTO `comments` VALUES (1,'コメント',2,2,'2018-04-09 04:02:52'),(2,'がんばれ',2,1,'2018-04-09 07:06:53'),(3,'１へのコメント',1,1,'2018-04-09 07:07:54'),(4,'コメント２',2,1,'2018-04-09 07:31:15'),(5,'komennto 2',1,1,'2018-04-09 07:32:10'),(6,'コメント３',2,1,'2018-04-09 07:32:42'),(7,'コメント４',2,1,'2018-04-09 07:35:11'),(8,'コメント５',1,1,'2018-04-09 07:35:25'),(9,'から',3,5,'2018-04-12 06:10:15'),(10,'こｒう',3,7,'2018-04-12 06:08:42'),(11,'コメント練習',10,1,'2018-04-17 07:15:59'),(12,'いいね！！！！！！！\r\n',11,1,'2018-04-18 02:13:54'),(13,'ああああああああああああ\r\nあああああああ',11,1,'2018-04-23 04:36:18'),(14,'コメント\r\n本文',16,2,'2018-04-24 04:49:55');
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(30) NOT NULL,
  `text` varchar(1000) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL,
  `category` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messages`
--

LOCK TABLES `messages` WRITE;
/*!40000 ALTER TABLE `messages` DISABLE KEYS */;
INSERT INTO `messages` VALUES (1,'四月六日','一回目の投稿','2018-04-06 06:42:49',1,'テスト'),(2,'二回目の投稿','明日までに終わらす。','2018-04-09 03:56:21',2,'テスト'),(3,'はははははは','はははじゃあっじゃあ','2018-04-16 01:21:41',1,'テスト'),(4,'　','　','2018-04-16 05:28:47',17,'　'),(6,'TEST','あああああ\r\nいいいい\r\nううう\r\nええ\r\nお','2018-04-16 09:47:46',7,'hoge'),(7,'TEST','<font color=\'red\'>hoge</font>','2018-04-16 09:48:22',7,'font'),(8,'tag','<script type=\"text/javascript\">\r\nalert(\"馬鹿\");\r\n</script>\r\n','2018-04-16 09:49:25',7,'tag'),(9,'tag','windows.alert(\"\");','2018-04-16 09:49:51',7,'tag'),(10,'tag','<script>\r\nwindows.alert(\"\");\r\n</script>','2018-04-16 09:50:12',7,'tag'),(11,'ニュース','2017年という年は、SKY-HIの活動がネクストフェイズに突入したことを明確に示す年となっている。年明けにアルバム『OLIVE』を発表し、それに伴う全国ツアーのファイナルでは初の日本武道館公演をいきなり2デイズで敢行。6月には、切れ味の鋭いリリックと、YouTubeのプライベートアカウントにて突然公開したことが大きな話題を呼んだ“キョウボウザイ”でその名をさらに知らしめ、ビートジャックの手法で自殺について言及した“0570-064-556”も同様に話題となった。その一方で、ロックフェスへの出演や、バンドとのコラボレーションを積極的に行い、ロックシーンのなかでの新たなポジションを徐々に構築。もちろん、AAAとしての活動も行っているわけで、そのバイタリティーには心底恐れ入る。\r\n\r\nそして、欧米・アジア公演を含む10月からのツアーを前に発表されるのが、新曲“Marble”だ。多様性を愛し、調和の美しさを説くこの曲は、これまで数々の分断を乗り越えてきたSKY-HIが歌うからこそ意味のある、新たな代表曲となり得る一曲だと思う。いつものようにジョークを交えつつも、エネルギッシュに、真摯に、2017年のこれまでを振り返る、SKY-HIの最新語録をお届けする。\r\n\r\nライブの最後に世界平和の話をするというのを武道館でやれたのは、意義深いことだった。\r\n―今日は2017年のここまでの活動を振り返ってもらいたいと思うのですが、まずは5月に行われた初の日本武道館公演の手応えから話していただけますか？\r\n\r\nSKY-HI：武道館は特別な場所だと思うし、特別なライブにもなりました。ただ、「あの瞬間に充実感が」というよりは、「積み上げてきてよかったな」って感じですね。自分、バンド、ダンサー、スタッフ全員の意識と、それを求めてくれるお客さんの意識、全部が同じ方向を向いた状態で武道館までやって来られたこと自体が素晴らしかったなと思います。「あのライブは武道館に見合ってた」っていう、それに尽きるかもしれない。','2018-04-18 02:13:32',1,'【ニュース】'),(12,'テスト','最近、SKY-HIというアーティストの名前をよく聞きませんか？「あー！あのラップしてるイケメン？」「フェスで見た」など出会い方は人それぞれだと思いますが、AAAの日高光啓といえば完全に人物が浮かんでくるでしょう。そんなSKY-HIは”2017年間違いなくブレイクするアーティストの一人”と音楽リスナーからの評価を高めています。\r\n\r\n2016年、SKY-HIの活動ペースは驚異的でした。シングル4枚(『アイリスライト』にてオリコンウィークリー2位を記録)とアルバム1枚をリリース。さらにストリートを席巻するラッパーSALUとのコラボレーション・アルバム『Say Hello to My Minions』をリリース。\r\nそして年間に2つの全国ツアーを実施。\r\n『SKY-HI LIVE HOUSE TOUR 2016 〜Round A Ground〜』では全国20箇所 21公演と全国のファンを熱狂させました。\r\nまた人気絶頂のバンドUVERworld、クリープハイプとのスペシャル対バンライブ。さらには国内最大級のロックフェス「COUNTDAWNJAPAN 1617」へ出演を果たし、自身のファンだけではなくロックリスナーの心も掴みとりました。\r\n\r\nそして2017年はすでにアルバムリリースと全国ツアーが決定し、極め付けは5月の日本武道館でのワンマン開催。今年は間違いなくSKY-HI旋風が巻き起ころうとしています。\r\nつまりは、今SKY-HIがキテます！\r\nミーティアではそんなSKY-HI（日高光啓）が今年ブレイクする理由を4つの魅力とともにまとめました。','2018-04-24 00:11:53',1,'ニュース'),(13,'テスト','今盛り上がりを見せる渋谷のサイファー(路上でフリースタイルの即興でラップバトルを行うコミュニティ)。この日も多くのラッパーが集まりラップバトルを繰り広げていた。その一角に佇む2人の怪しいラッパーの映像が話題となっている。\r\nフリースタイルダンジョンでもおなじみACEらが主催するこの渋谷サイファーの輪の中に飛び入り参加する2人の怪しいラッパーだがお世辞にもラップスキルが高いとは言えない。その光景に集まった参加者も半分呆れた顔を浮かべているた。しかしその2人の巧みなライムとフローで観客を煽っていく。\r\nすると、片方のラッパーが帽子とサングラスとつけ髭を投げ捨てた。\r\nそのラッパーの正体はSKY-HI。\r\nそして、もう片方のラッパーも帽子とサングラスとつけ髭を脱ぎ捨てると、\r\nその正体はラッパーのSALU。怪しい2人組の正体は、なんと、変装していたSKY-HIとSALUだった。このサプライズ登場に、観客からは大きな歓声が上がった。\r\n\r\nこれは10月26日(水)に発売される2人のコラボレーションアルバム\r\n『Say Hello to My Minions』のミュージックビデオの映像用に収録されたサプライズ企画。\r\nSKY-HIとSALUの登場に、渋谷の公園に集結していた観客は騒然。\r\n2人が繰り広げる即興ラップに最高の歓声が鳴りやまない状態となった。\r\n\r\n【SKY-HI × SALU / Sleep Walking】 Music Video メイキング (渋谷サイファー)\r\n\r\n\r\nコラボレーション・アルバム『Say Hello to My Minions』に収録される「Sleep Walking」は「眠っている人を起こす」為に2人が街にでかけると言う『コンセプトの内容になっており、東京の夜景をバックに、2人の動きが絶妙にシンクロするリップシーンの他、フリースタイルダンジョンで活躍中のラッパー・ACE\r\n率いる渋谷サイファーに、一般人に変装した2人が潜入し、フリースタイルができない”怪しいヤツ”を演じ嘲笑を浴びた後、\r\n変装をとって正体を明かし、驚きの歓声が上がる中、 心掴まれるワードセンスとキレキレなフリースタイルの応酬で観衆を盛り上げた\r\n実際の映像記録が盛り込まれた作品になっている。','2018-04-24 00:14:50',2,'テスト'),(15,'コメント練習件名','コメントの練習。\r\n本文→→','2018-04-24 00:51:03',2,'コメントテスト'),(16,'発表練習','発表練習の本文。\r\n本文','2018-04-24 04:48:37',2,'テスト');
/*!40000 ALTER TABLE `messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `positions`
--

DROP TABLE IF EXISTS `positions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `positions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `position_name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `positions`
--

LOCK TABLES `positions` WRITE;
/*!40000 ALTER TABLE `positions` DISABLE KEYS */;
INSERT INTO `positions` VALUES (1,'総務部'),(2,'支店長'),(3,'社員');
/*!40000 ALTER TABLE `positions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login_id` varchar(20) NOT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(10) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `position_id` int(11) NOT NULL,
  `is_deleted` int(11) NOT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `login_id` (`login_id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'osamu01','OmkT2mz2WkNCq8aVrlGPfOUy5tKCpo0eiSXudLgj108','オサム１',1,1,1,'2018-04-24 07:28:10','2018-04-24 07:28:10'),(2,'osamu02','GDPVSSnAmV7f8C_1VQ-66Y6RK_1DY32uuzWPuu9tWOY','おさむ２',4,2,1,'2018-04-24 00:22:57','2018-04-24 00:22:57'),(4,'osamu03','OMYXS_J_ClJlJbW4EJgk-fPaM1lX3dz7bIJ8AYUdEMY','おさむ３',3,3,1,'2018-04-19 04:27:06','2018-04-16 07:43:40'),(5,'osamu05','_vLADas3IbijCEZTtL77YdC1SwU35eDc_mVS0_3g53M','おさむ５',4,4,1,'2018-04-12 07:16:22','2018-04-03 01:58:05'),(6,'osamu06','1AMKLbkiSnvM8gWbnNmfVqZGSE-7JNY0o1em6wdzxlk','おさむ６',1,1,0,'2018-04-24 05:05:47','2018-04-16 07:32:38'),(7,'admin01','CHbfym1v7fmbKrh7bi_tS9QFHt54qKkTW1ALLpTZm4g','総務人事部ｗ',1,1,1,'2018-04-24 08:24:18','2018-04-24 08:24:18'),(8,'osamu07','0nFwVGvUNC0McO2lMKEMcqC9Cr_vJbjJ7sMcrdm5mQs','おさむ０７',1,1,1,'2018-04-17 04:12:56','2018-04-13 07:10:45'),(9,'osamu08','qWQNTS51hJJKNiO5x_Dp1N6tEANGysM-ZaB7HbBnjKc','おさむ８',1,1,1,'2018-04-23 04:12:35','2018-04-20 04:32:04'),(10,'osamu09','E9aXtpgSr-dRmKHlSE0ZcIM6MLytGgzfE4mVqJRAyok','治九',1,1,1,'2018-04-15 23:47:56','2018-04-13 07:44:12'),(13,'osamu10','8vDaWweG1cXL3axkOG9onqqSZsP722hRDRPK6XHz7Hk','治十',1,1,0,'2018-04-23 04:01:26','2018-04-13 08:50:49'),(14,'osamu11','DOo5jYvxMBebU6UFv9c225Zp1BTlq9lkBQDgxcuZ8-E','十一朗',1,1,1,'2018-04-15 23:48:01','2018-04-13 08:54:21'),(15,'osamu12','CwBK2xFwtaO9aSeRHncl1ciqjkloWpHanZiacFw1XB0','１２朗',1,1,1,'2018-04-24 04:37:10','2018-04-24 04:37:10'),(16,'osamu13','_p1RWX_Fj5yTEUfgpDuGZtQ5msYnqZ6oUv4TgksuPEI','おさむ１３',1,1,1,'2018-04-16 02:33:21','2018-04-16 02:19:54'),(17,'admin02','DMv0VaLCjthl7gzBb5tTFAXXvXX4fa0tYsbWRBc70r8','めいめい',2,3,0,'2018-04-23 04:00:27','2018-04-16 07:59:24'),(18,'osamu14','l9Mf4MpEa5DiNx1-ob3z0Bk8HM5ZKrI0KO-iePH1LQQ','おさむ１４',3,2,1,'2018-04-16 07:55:59','2018-04-16 07:55:59'),(19,'osamu15','Dfbe21NL3ogflaEgckuyw237T6vcQGK_IilWkzBKlEk','おさむ15',2,1,1,'2018-04-16 07:56:03','2018-04-16 07:56:03'),(21,'admin03','3y8HfORkNCr74TtX8hEFx14Y_wiDR1m5_a53Bu0ppPU','パスワードが',4,2,1,'2018-04-16 09:55:14','2018-04-16 09:55:14'),(22,'test02','4cEIBgJ2N1d2U6JRIOSoQ3vlIHwuJQGBgjpPwyPq7jk','テスト02',1,1,1,'2018-04-24 05:01:14','2018-04-24 05:01:14'),(23,'test002','4cEIBgJ2N1d2U6JRIOSoQ3vlIHwuJQGBgjpPwyPq7jk','テスト２',1,1,1,'2018-04-24 04:56:24','2018-04-24 04:56:24');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-04-27 15:27:52
