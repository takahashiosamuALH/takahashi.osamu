package takahashi_osamu.dao;

import static takahashi_osamu.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import takahashi_osamu.beans.Position;
import takahashi_osamu.exception.SQLRuntimeException;

public class PositionDao {

	public List<Position> getPositionList(Connection connection) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM positions ";

			ps = connection.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();
			List<Position> positionList = toPositionList(rs);
			if (positionList.isEmpty() == true) {
				return null;
			} else {
				return positionList;
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<Position> toPositionList(ResultSet rs) throws SQLException {

		List<Position> ret = new ArrayList<Position>();
		try {
			while (rs.next()) {
				Integer id = rs.getInt("id");
				String position_name = rs.getString("position_name");
				Position position = new Position();
				position.setId(id);
				position.setPosition_name(position_name);

				ret.add(position);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

}
