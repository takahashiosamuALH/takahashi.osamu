package takahashi_osamu.dao;

import static takahashi_osamu.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import takahashi_osamu.beans.Branch;
import takahashi_osamu.beans.User;
import takahashi_osamu.exception.SQLRuntimeException;

public class BranchDao {

	public List<Branch> getBranchList(Connection connection) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM branches ";

			ps = connection.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();
			List<Branch> branchList = toBranchList(rs);
			if (branchList.isEmpty() == true) {
				return null;
			} else {
				return branchList;
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<Branch> toBranchList(ResultSet rs) throws SQLException {

		List<Branch> ret = new ArrayList<Branch>();
		try {
			while (rs.next()) {
				Integer id = rs.getInt("id");
				String branch_name = rs.getString("branch_name");
				Branch branch = new Branch();
				branch.setId(id);
				branch.setBranch_name(branch_name);

				ret.add(branch);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

}
