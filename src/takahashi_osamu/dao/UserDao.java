package takahashi_osamu.dao;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import static takahashi_osamu.utils.CloseableUtil.*;
import static takahashi_osamu.utils.CloseableUtil.*;
import takahashi_osamu.beans.User;
import takahashi_osamu.exception.NoRowsUpdatedRuntimeException;
import takahashi_osamu.exception.SQLRuntimeException;

public class UserDao {

	public void insert(Connection connection, User user) {
		PreparedStatement ps = null;

		try {

			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO users ( ");
			sql.append("login_id");
			sql.append(", password");
			sql.append(", name");
			sql.append(", branch_id");
			sql.append(", position_id");
			sql.append(", created_at");
			sql.append(", updated_date");
			sql.append(", is_deleted");

			sql.append(") VALUES (");
			sql.append("?"); // login_id
			sql.append(", ?"); // passsword
			sql.append(", ?"); // name
			sql.append(", ?"); // branch_id
			sql.append(", ?"); // position_id
			sql.append(", CURRENT_TIMESTAMP"); // created_at
			sql.append(", CURRENT_TIMESTAMP"); // updated_date
			sql.append(", '1' "); // is_deleted
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			System.out.println(sql.toString());

			ps.setString(1, user.getLogin_id());
			ps.setString(2, user.getPassword());
			ps.setString(3, user.getName());
			ps.setInt(4, user.getBranch_id());
			ps.setInt(5, user.getPosition_id());

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public User getLoginIdCheck(Connection connection, String login_id) {

		PreparedStatement ps = null;
		try {

			String sql = "SELECT * FROM users WHERE login_id = ?";

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, login_id);

			ResultSet rs = ps.executeQuery();

			List<User> userSetting = toUserList(rs);

			if (userSetting.isEmpty() == true) {
				return null;
			} else if (2 <= userSetting.size()) {
				throw new IllegalStateException("2 <= userSetting.size()");
			} else {
				return userSetting.get(0);
			}

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}




	public User getUser(Connection connection, String login_id, String password) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE (login_id = ?) AND (password = ?) AND (is_deleted = 1) ";

			ps = connection.prepareStatement(sql);
			ps.setString(1, login_id);
			ps.setString(2, password);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if (userList.isEmpty() == true) {
				return null;
			} else if (2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}



	public List<User> getUsersList(Connection connection) throws SQLException {

		PreparedStatement ps = null;
		try {

			int LIMIT_NUM = 1000;

			String sql = "SELECT users.id as id, users.name as name, users.is_deleted as is_deleted, users.login_id as login_id, "
					+ "branches.branch_name as branch_name, positions.position_name as position_name, users.created_at as created_at "
					+ "FROM users INNER JOIN branches ON branches.id = users.branch_id "
					+ "INNER JOIN positions ON positions.id = users.position_id ORDER BY branch_id ASC, position_id ASC, login_id ASC";
//					+ "ORDER BY branch_id ASC ;";

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();

			List<User> usersList = toUserDataList(rs);

			return usersList;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}



	public User getUserSettings(Connection connection, int id) {

		PreparedStatement ps = null;
		try {



			String sql = "SELECT * FROM users WHERE id = ?";

			ps = connection.prepareStatement(sql.toString());
			ps.setInt(1, id);

			ResultSet rs = ps.executeQuery();

			List<User> userSetting = toUserList(rs);

			if (userSetting.isEmpty() == true) {
				return null;
			} else if (2 <= userSetting.size()) {
				throw new IllegalStateException("2 <= userSetting.size()");
			} else {
				return userSetting.get(0);
			}

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}







	private List<User> toUserList(ResultSet rs) throws SQLException {

		List<User> ret = new ArrayList<User>();
		try {
			while (rs.next()) {
				Integer id = rs.getInt("id");
				String login_id = rs.getString("login_id");
				String name = rs.getString("name");
				Integer branch_id = rs.getInt("branch_id");
				Integer position_id = rs.getInt("position_id");
				Integer is_deleted = rs.getInt("is_deleted");
				User user = new User();
				user.setId(id);
				user.setLogin_id(login_id);
				user.setName(name);
				user.setBranch_id(branch_id);
				user.setPosition_id(position_id);
				user.setIs_deleted(is_deleted);

				ret.add(user);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	private List<User> toUserDataList(ResultSet rs) throws SQLException {

		List<User> ret = new ArrayList<User>();
		try {
			while (rs.next()) {
				Integer id = rs.getInt("id");
				String login_id = rs.getString("login_id");
				String name = rs.getString("name");
				String branch_name = rs.getString("branch_name");
				String position_name = rs.getString("position_name");
				Integer is_deleted = rs.getInt("is_deleted");
				User user = new User();
				user.setId(id);
				user.setLogin_id(login_id);
				user.setName(name);
				user.setBranch_name(branch_name);
				user.setPosition_name(position_name);
				user.setIs_deleted(is_deleted);

				ret.add(user);
			}
			return ret;
		} finally {
			close(rs);
		}
	}



	public void update(Connection connection, User user) {
		PreparedStatement ps = null;

		if(StringUtils.isBlank(user.getPassword())){
			try {
				StringBuilder sql = new StringBuilder();
				sql.append("UPDATE users SET");
				sql.append("  login_id = ?");
				sql.append(", name = ?");
				sql.append(", branch_id = ?");
				sql.append(", position_id = ?");
				sql.append(", created_at = CURRENT_TIMESTAMP");
				sql.append(" WHERE");
				sql.append(" id = ?");

				ps = connection.prepareStatement(sql.toString());

				ps.setString(1, user.getLogin_id());
				ps.setString(2, user.getName());
				ps.setInt(3, user.getBranch_id());
				ps.setInt(4, user.getPosition_id());
				ps.setInt(5, user.getId());


				int count = ps.executeUpdate();
				if (count == 0) {

					throw new NoRowsUpdatedRuntimeException();
				}

			} catch (SQLException e) {
				throw new SQLRuntimeException(e);
			} finally {
				close(ps);
			}
		} else {
			try {
				StringBuilder sql = new StringBuilder();
				sql.append("UPDATE users SET");
				sql.append("  login_id = ?");
				sql.append(", password = ?");
				sql.append(", name = ?");
				sql.append(", branch_id = ?");
				sql.append(", position_id = ?");
				sql.append(", created_at = CURRENT_TIMESTAMP");
				sql.append(" WHERE");
				sql.append(" id = ?");

				ps = connection.prepareStatement(sql.toString());

				ps.setString(1, user.getLogin_id());
				ps.setString(2, user.getPassword());
				ps.setString(3, user.getName());
				ps.setInt(4, user.getBranch_id());
				ps.setInt(5, user.getPosition_id());
				ps.setInt(6, user.getId());


				int count = ps.executeUpdate();
				if (count == 0) {

					throw new NoRowsUpdatedRuntimeException();
				}

			} catch (SQLException e) {
				throw new SQLRuntimeException(e);
			} finally {
				close(ps);
			}
		}
	}


	public void onoff(Connection connection, User user) {
		PreparedStatement ps = null;

		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET");
			sql.append(" is_deleted = ?");
			sql.append(" WHERE");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, user.getIs_deleted());
			ps.setInt(2, user.getId());

			int count = ps.executeUpdate();
			if (count == 0) {

				throw new NoRowsUpdatedRuntimeException();
			}

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}

}
