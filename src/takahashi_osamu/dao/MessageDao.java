package takahashi_osamu.dao;

import static takahashi_osamu.utils.CloseableUtil.*;
import static takahashi_osamu.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import takahashi_osamu.beans.Message;
import takahashi_osamu.exception.NoRowsUpdatedRuntimeException;
import takahashi_osamu.exception.SQLRuntimeException;



public class MessageDao {


	public void insert(Connection connection, Message message) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO messages ( ");
			sql.append("user_id");
			sql.append(", title");
			sql.append(", text");
			sql.append(", category");
			sql.append(", created_at");
			sql.append(") VALUES (");
			sql.append(" ?"); // user_id     投稿者
			sql.append(", ?"); // title
			sql.append(", ?"); // text
			sql.append(", ?"); // category
			sql.append(", CURRENT_TIMESTAMP"); // created_at
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, message.getUserId());
			ps.setString(2, message.getTitle());
			ps.setString(3, message.getText());
			ps.setString(4, message.getCategory());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public void delete(Connection connection, int deleteMessage) {

		PreparedStatement ps = null;
		try {

			System.out.println(deleteMessage + "ああああ");

			String sql = "DELETE FROM messages WHERE id = ?";

			ps = connection.prepareStatement(sql.toString());
			ps.setInt(1, deleteMessage);
			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
}
