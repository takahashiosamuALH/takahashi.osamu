package takahashi_osamu.dao;

import static takahashi_osamu.utils.CloseableUtil.*;
import static takahashi_osamu.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.StringUtils;

import takahashi_osamu.beans.UserMessage;
import takahashi_osamu.exception.SQLRuntimeException;

public class UserMessageDao {

	public List<UserMessage> getUserMessages(Connection connection, int num) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("messages.id as id, ");
			sql.append("messages.title as title, ");
			sql.append("messages.text as text, ");
			sql.append("messages.user_id as user_id, ");
			sql.append("users.login_id as login_id, ");
			sql.append("users.name as name, ");
			sql.append("messages.created_at as created_at, ");
			sql.append("messages.category as category ");
			sql.append("FROM messages ");
			sql.append("INNER JOIN users ");
			sql.append("ON messages.user_id = users.id ");
			sql.append("ORDER BY created_at DESC limit " + num);

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<UserMessage> ret = toUserMessageList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public List<UserMessage> getSearch(Connection connection, String dateStart, String dateGoal, String category, int num) {


		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("messages.id as id, ");
			sql.append("messages.title as title, ");
			sql.append("messages.text as text, ");
			sql.append("messages.user_id as user_id, ");
			sql.append("users.login_id as login_id, ");
			sql.append("users.name as name, ");
			sql.append("messages.created_at as created_at, ");
			sql.append("messages.category as category ");
			sql.append("FROM messages ");
			sql.append("INNER JOIN users ");
			sql.append("ON messages.user_id = users.id ");
			sql.append("WHERE messages.created_at >= ? AND messages.created_at <= ? ");
			if(!StringUtils.isNullOrEmpty(category)){
				sql.append("AND messages.category LIKE ?");
			}
			sql.append("ORDER BY created_at DESC limit " + num);

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, dateStart + " 00:00:00");
			ps.setString(2, dateGoal + "23:59:59");
			if(!StringUtils.isNullOrEmpty(category)){
				ps.setString(3, "%" + category + "%");
			}

			ResultSet rs = ps.executeQuery();
			List<UserMessage> ret = toUserMessageList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}





	private List<UserMessage> toUserMessageList(ResultSet rs)throws SQLException {

		List<UserMessage> ret = new ArrayList<UserMessage>();
		try {
			while (rs.next()) {
				String login_id = rs.getString("login_id");
				String name = rs.getString("name");
				int id = rs.getInt("id");
				int userId = rs.getInt("user_id");
				String title = rs.getString("title");
				String text = rs.getString("text");
				Timestamp created_at = rs.getTimestamp("created_at");
				String category = rs.getString("category");

				UserMessage message = new UserMessage();
				message.setLogin_id(login_id);
				message.setName(name);
				message.setId(id);
				message.setUser_id(userId);
				message.setTitle(title);
				message.setText(text);
				message.setCreated_at(created_at);
				message.setCategory(category);


				ret.add(message);
			}
			return ret;
		} finally {
			close(rs);
		}
	}





}
