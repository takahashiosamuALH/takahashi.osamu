package takahashi_osamu.servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import takahashi_osamu.beans.Branch;
import takahashi_osamu.beans.User;
import takahashi_osamu.exception.NoRowsUpdatedRuntimeException;
import takahashi_osamu.service.UserService;


//  usercontrol で呼び出される
@WebServlet(urlPatterns = { "/usercontrol" })
public class UserControlServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	//  doGetでサーバーに対してページの取得を要求する
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		try {
			List<User>  users = new UserService().getUsersList();

			request.setAttribute("users", users);

			//  Dispatcher で usercontrol.jsp へ。 forward(Dispatcher でjspに移るタイミング)に実行
			request.getRequestDispatcher("usercontrol.jsp").forward(request, response);
		} catch (SQLException e) {
			throw new ServletException(e);
		}
	}
	@Override
	//  doPostで method="POST"を指定したフォームに入力したデータをサーバーに転送する
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException {

		User userCondition = getUsersCondition(request);

		try {
			List<User>  users = new UserService().getUsersList();
			request.setAttribute("users", users);
			new UserService().usersCondition(userCondition);
		} catch (NoRowsUpdatedRuntimeException e) {
			request.setAttribute("userCondition", userCondition);
			request.getRequestDispatcher("usercontrol.jsp").forward(request, response);
			return;
		} catch (SQLException e) {
			throw new ServletException(e);
		}
		response.sendRedirect("usercontrol");
	}

	private User getUsersCondition(HttpServletRequest request)throws IOException, ServletException {

		//  一時保持
		User usersCondition = new User();
		usersCondition.setId(Integer.parseInt(request.getParameter("id")));
		usersCondition.setIs_deleted(Integer.parseInt(request.getParameter("is_deleted")));

		return usersCondition;
	}

	private Branch getBranchList(HttpServletRequest request)throws IOException, ServletException {

		//  一時保持
		Branch branch_id = new Branch();
		branch_id.setId(Integer.parseInt(request.getParameter("id")));
		branch_id.setBranch_name(request.getParameter("is_deleted"));
		request.setAttribute("branch_id", branch_id);

		return branch_id;
	}


}
