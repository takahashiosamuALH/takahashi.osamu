package takahashi_osamu.servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import takahashi_osamu.beans.Branch;
import takahashi_osamu.beans.Position;
import takahashi_osamu.beans.User;
import takahashi_osamu.exception.NoRowsUpdatedRuntimeException;
import takahashi_osamu.service.UserService;

@WebServlet(urlPatterns = { "/signup" })
// HttpServlet を継承
public class SignUpServlet extends HttpServlet {
	 private static final long serialVersionUID = 1L;

	@Override
	//  doGetでサーバーに対してページの取得を要求する
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		try {
			List<Branch> branch = new UserService().getBranchList();
			List<Position> position = new UserService().getPositionList();

			request.setAttribute("branch", branch);
			request.setAttribute("position", position);
		} catch (SQLException e) {
			return;
		}
		//  Dispatcher で singup.jsp へ。 forward(Dispatcher でjspに移るタイミング)に実行
		 request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

	@Override
	//  doPostで method="POST"を指定したフォームに入力したデータをサーバーに転送する
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException {

		try {
			User loginIdCheck = new UserService().getLoginIdCheck(request.getParameter("login_id"));
			request.setAttribute("loginIdCheck", loginIdCheck);
		} catch (SQLException e1) {
			// TODO 自動生成された catch ブロック
			e1.printStackTrace();
		}


		//  ArrayList の messages を宣言
		List<String> messages = new ArrayList<String>();
			try {
				List<Branch> branch = new UserService().getBranchList();
				List<Position> position = new UserService().getPositionList();
				request.setAttribute("branch", branch);
				request.setAttribute("position", position);
			} catch (SQLException e) {
				return;
			}

		HttpSession session = request.getSession();

		//  isValid()で true なら・・・
		User user = getUser(request);

		if (isValid(request, messages) == true) {

            new UserService().register(user);

            // 値を持って前のページへ戻る
            response.sendRedirect("usercontrol");

		} else {

			//  set された errorMessage
			session.setAttribute("errorMessages", messages);
			request.setAttribute("user", user);
			request.getRequestDispatcher("signup.jsp").forward(request, response);;

		}
	}

	private User getUser(HttpServletRequest request) throws IOException, ServletException {

		User user = new User();
		user.setName(request.getParameter("name"));
		user.setLogin_id(request.getParameter("login_id"));
		user.setPassword(request.getParameter("password"));
		user.setName(request.getParameter("name"));
		user.setBranch_id(Integer.parseInt(request.getParameter("branch_id")));
		user.setPosition_id(Integer.parseInt(request.getParameter("position_id")));

        return user;
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {
		//  エラーメッセージ

		try {
			User loginIdCheck = new UserService().getLoginIdCheck(request.getParameter("login_id"));
			request.setAttribute("loginIdCheck", loginIdCheck);
			if(loginIdCheck != null) {
				messages.add("ログインＩＤが重複しています。");
			}
		} catch (SQLException e1) {
			// TODO 自動生成された catch ブロック
			e1.printStackTrace();
		}




		String loginId = request.getParameter("login_id");
		String password = request.getParameter("password");
		String passwordcheck = request.getParameter("passwordcheck");
		String name = request.getParameter("name");
		String branchId = request.getParameter("branch_id");
		String positionId = request.getParameter("position_id");


		if (StringUtils.isEmpty(loginId) == true) {
			messages.add("ログインＩＤを入力してください");
		} else if (loginId.length() < 6 || loginId.length() > 20) {
			messages.add("ログインＩＤは6文字以上、20以下で入力してください");
		} else if (!(loginId).matches("^[0-9a-zA-Z]+$")) {
			messages.add("ログインＩＤは半角英数で入力してください");
		}

		if (StringUtils.isEmpty(password) == true) {
			messages.add("パスワードを入力してください");
		} else if (password.length() < 6 || password.length() > 20) {
			messages.add("パスワードは6文字以上、20以下で入力してください。");
		} else if (!(password).matches("^[0-9a-zA-Z]+$")) {
			messages.add("パスワードは半角英数で入力してください");
		}

		if (StringUtils.equals(password, passwordcheck) == false) {
			messages.add("パスワードが一致しません");
		}

		if (StringUtils.isEmpty(name) == true) {
			messages.add("名前を入力してください");
		} else if (name.length() > 10) {
			messages.add("名前は10以下で入力してください。");
		}

		if (StringUtils.equals(branchId, "0") == true) {
			messages.add("支店を選択してください");
		}

		if (StringUtils.equals(positionId, "0") == true) {
			messages.add("部署・役職を選択してください");
		}

		if (messages.size() == 0) {
			return true;
		} else {
            return false;
        }
	}
}
