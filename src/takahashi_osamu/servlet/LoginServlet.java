package takahashi_osamu.servlet;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import takahashi_osamu.beans.User;
import takahashi_osamu.service.LoginService;




@WebServlet(urlPatterns = { "/login" })
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		request.getRequestDispatcher("login.jsp").forward(request, response);
		}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		String login_id = request.getParameter("login_id");
		String password = request.getParameter("password");

		LoginService loginService = new LoginService();
		User loginUser = loginService.login(login_id, password);

//		request.setAttribute("loginId", login_id);

		User user = getUser(request);
		request.setAttribute("user", user);

		HttpSession session = request.getSession();

		if (loginUser != null) {
			session.setAttribute("loginUser", loginUser);
			response.sendRedirect("./");
		} else {
			List<String> messages = new ArrayList<String>();
            messages.add("ログインに失敗しました。");
            session.setAttribute("errorMessages", messages);
            request.getRequestDispatcher("login.jsp").forward(request, response);
		}

	}
	private User getUser(HttpServletRequest request) throws IOException, ServletException {

		User user = new User();
		user.setLogin_id(request.getParameter("login_id"));
		user.setPassword(request.getParameter("password"));

        return user;
	}
}
