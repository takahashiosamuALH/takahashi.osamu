package takahashi_osamu.servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import takahashi_osamu.beans.Branch;
import takahashi_osamu.beans.Position;
import takahashi_osamu.beans.User;
import takahashi_osamu.exception.NoRowsUpdatedRuntimeException;
import takahashi_osamu.service.UserService;
//import takahashi_osamu.beans.UserMessage;
//import takahashi_osamu.dao.UserMessageDao;

import org.apache.commons.lang.StringUtils;

@WebServlet(urlPatterns = { "/settings" })
public class SettingsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();
		try{
			if (StringUtils.isBlank(request.getParameter("id"))) {
				messages.add("不正なパラメータです");
				session.setAttribute("errorMessages", messages);
				response.sendRedirect("usercontrol");
				return;
			}

			if (!request.getParameter("id").matches("^[0-9]+$")) {
				messages.add("不正なパラメータです");
				session.setAttribute("errorMessages", messages);
				response.sendRedirect("usercontrol");
				return;
			}

			//ログインユーザー情報のidを元にDBからユーザー情報取得
			User editUser =new UserService().getUser(Integer.parseInt(request.getParameter("id")));

			if(editUser == null){
				messages.add("不正なパラメータです");
				session.setAttribute("errorMessages", messages);
				response.sendRedirect("usercontrol");
				return;
			}

			List<Branch> branch = new UserService().getBranchList();
			List<Position> position = new UserService().getPositionList();
			request.setAttribute("branch", branch);
			request.setAttribute("position", position);
			request.setAttribute("editUser", editUser);

			request.getRequestDispatcher("settings.jsp").forward(request, response);
		} catch (SQLException e) {
			throw new ServletException(e) ;
		}
	}


	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		List<String> messages = new ArrayList<String>();
		User editUser = getUserItems(request);
		HttpSession session = request.getSession();

		try {
			List<Branch> branch = new UserService().getBranchList();
			List<Position> position = new UserService().getPositionList();
			request.setAttribute("branch", branch);
			request.setAttribute("position", position);
		} catch (SQLException e) {
			return;
		}

		if (isValid(request, messages) == true) {

			try {
				new UserService().update(editUser);
			} catch (NoRowsUpdatedRuntimeException e) {
				request.setAttribute("editUser", editUser);
				request.getRequestDispatcher("settings.jsp").forward(request, response);
				return;
			}
			response.sendRedirect("usercontrol");

		} else {
			session.setAttribute("errorMessages", messages);
			request.setAttribute("editUser", editUser);
			request.getRequestDispatcher("settings.jsp").forward(request, response);
		}
	}

	private User getUserItems(HttpServletRequest request)
			throws IOException, ServletException {

		String password = request.getParameter("password");
		String branch_id = request.getParameter("branch_id");
		String position_id = request.getParameter("position_id");

		User userItems = new User();
		userItems.setId(Integer.parseInt(request.getParameter("id")));
		userItems.setLogin_id(request.getParameter("login_id"));
		if(password.length() != 0){
			userItems.setPassword(request.getParameter("password"));
		} else {
			userItems.setPassword(null);
		}
		userItems.setName(request.getParameter("name"));

		if(!StringUtils.isEmpty(branch_id)){
			userItems.setBranch_id(Integer.parseInt(request.getParameter("branch_id")));
		} else {
			userItems.setBranch_id(1);
		}

		if(!StringUtils.isEmpty(position_id)){
			userItems.setPosition_id(Integer.parseInt(request.getParameter("position_id")));
		} else {
			userItems.setPosition_id(1);
		}
		return userItems;
    }

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		try {

			User editUser =new UserService().getUser(Integer.parseInt(request.getParameter("id")));
			User loginIdCheck = new UserService().getLoginIdCheck(request.getParameter("login_id"));
			request.setAttribute("loginIdCheck", loginIdCheck);


			if(!StringUtils.equals(editUser.getLogin_id(), request.getParameter("login_id"))){
				if(loginIdCheck != null) {
					messages.add("ログインＩＤが重複しています。");
				}
			}
		} catch (SQLException e1) {
			// TODO 自動生成された catch ブロック
			e1.printStackTrace();
		}


		String login_id = request.getParameter("login_id");
		String password = request.getParameter("password");
		String passwordcheck = request.getParameter("passwordcheck");
		String name = request.getParameter("name");
		String branch_id = request.getParameter("branch_id");
		String position_id = request.getParameter("position_id");

		if (StringUtils.isEmpty(login_id) == true) {
			messages.add("ログインＩＤを入力してください");
		} else if(login_id.length() < 6 || login_id.length() > 20) {
			messages.add("ログインＩＤは6文字以上、20以下で入力してください。");
		} else if (!(login_id).matches("^[0-9a-zA-Z]+$")) {
			messages.add("ログインＩＤは半角英数で入力してください");
		}

		if (!StringUtils.isEmpty(password)) {
			if (password.length() < 6 || password.length() > 20) {
				messages.add("パスワードは6文字以上、20以下で入力してください。");
			} else if (!(password).matches("^[0-9a-zA-Z]+$")) {
				messages.add("パスワードは半角英数で入力してください");
			}
		} else if (!StringUtils.isEmpty(passwordcheck)) {
			if (password.length() < 6 || passwordcheck.length() > 20) {
				messages.add("パスワードは6文字以上、20以下で入力してください。");
			} else if (!(passwordcheck).matches("^[0-9a-zA-Z]+$")) {
				messages.add("パスワードは半角英数で入力してください");
			}
		}




		if (StringUtils.isEmpty(name) == true) {
			messages.add("名前を入力してください");
		} else if (name.length() > 10) {
			messages.add("名前は10以下で入力してください。");
		}

		if (StringUtils.equals(branch_id, "0") == true) {
			messages.add("支店を選択してください");
		}

		if (StringUtils.equals(position_id, "0") == true) {
			messages.add("部署・役職を選択してください");
		}

		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}
