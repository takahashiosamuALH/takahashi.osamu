package takahashi_osamu.servlet;


import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mysql.jdbc.StringUtils;

import takahashi_osamu.beans.User;
import takahashi_osamu.beans.UserComment;
import takahashi_osamu.beans.UserMessage;
import takahashi_osamu.exception.NoRowsUpdatedRuntimeException;
import takahashi_osamu.service.CommentService;
import takahashi_osamu.service.MessageService;
import takahashi_osamu.service.UserService;


@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

    	request.setCharacterEncoding("UTF-8");

    	User user = (User) request.getSession().getAttribute("loginUser");
    	//TODO
    	HttpSession session = request.getSession();

		List<UserMessage> messages = new MessageService().getMessage();
		request.setAttribute("messages", messages);

		List<UserComment> comment = new CommentService().getComment();
		request.setAttribute("comments", comment);

		String from = request.getParameter("dateStart");
		String to = request.getParameter("dateGoal");
		String category = request.getParameter("category");
		request.setAttribute("from", from);
		request.setAttribute("to", to);
		request.setAttribute("category", category);

		if(StringUtils.isNullOrEmpty(from)){
			from = "1500-01-01";
		}
		if(StringUtils.isNullOrEmpty(to)) {
			Date now = new Date();
			SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy'-'MM'-'dd");
			to = (sdf1.format(now));
		}

		new MessageService().getSearch(from, to, category) ;
		List<UserMessage> dateSearch = new MessageService().getSearch(from, to, category);
		request.setAttribute("messages", dateSearch);



		request.getRequestDispatcher("/top.jsp").forward(request, response);



    }
}
