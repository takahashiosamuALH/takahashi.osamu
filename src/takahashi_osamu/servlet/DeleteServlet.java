package takahashi_osamu.servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import takahashi_osamu.beans.Comment;
import takahashi_osamu.beans.Message;
import takahashi_osamu.beans.User;
import takahashi_osamu.exception.NoRowsUpdatedRuntimeException;
import takahashi_osamu.service.CommentService;
import takahashi_osamu.service.MessageService;
import takahashi_osamu.service.UserService;


@WebServlet(urlPatterns = { "/delete"})
public class DeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException {

		if((request.getParameter("message_id")) != null){

			int deleteMessage = Integer.parseInt(request.getParameter("message_id"));
			new MessageService().deleteMessage(deleteMessage);
			response.sendRedirect("./");
		}

		if (request.getParameter("comment_id") != null){

			int deleteComment = Integer.parseInt(request.getParameter("comment_id"));
				new CommentService().deleteComment(deleteComment);
				response.sendRedirect("./");
		}
	}
}
