package takahashi_osamu.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import takahashi_osamu.beans.Comment;
import takahashi_osamu.beans.User;
import takahashi_osamu.service.CommentService;
import takahashi_osamu.service.MessageService;

@WebServlet(urlPatterns = { "/comment" })
public class NewCommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		request.getRequestDispatcher("top.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();

		List<String> messages = new ArrayList<String>();

		if (isValid(request, messages)) {

			User user = (User) session.getAttribute("loginUser");

			Comment comment = new Comment();
			comment.setText(request.getParameter("text"));
			comment.setUserId(user.getId());
			comment.setMessageId(Integer.parseInt(request.getParameter("message_id")));

			new CommentService().register(comment);

			response.sendRedirect("./");
		} else {
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("./");
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String text = request.getParameter("text");

		if (StringUtils.isBlank(text)) {
			messages.add("コメントを入力してください");
		}

		if (500 < text.length()) {
			messages.add("コメントは500文字以下で入力してください");
		}

        if (messages.size() == 0) {
            return true;
        } else {

            return false;
        }

	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}


}
