package takahashi_osamu.beans;

import java.io.Serializable;

public class UserBranch implements Serializable {

	private static final long serialVersionUID = 1L;

	private int id;
	private String branch_name;
	private int branch_id;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getBranch_name() {
		return branch_name;
	}
	public void setBranch_name(String branch_name) {
		this.branch_name = branch_name;
	}
	public int getBranch_id() {
		return branch_id;
	}
	public void setBranch_id(int branch_id) {
		this.branch_id = branch_id;
	}



}
