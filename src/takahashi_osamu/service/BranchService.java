package takahashi_osamu.service;

import static takahashi_osamu.utils.CloseableUtil.*;
import static takahashi_osamu.utils.DBUtil.*;

import java.sql.Connection;

import takahashi_osamu.beans.Branch;
import takahashi_osamu.beans.User;
import takahashi_osamu.dao.BranchDao;
import takahashi_osamu.dao.UserDao;
import takahashi_osamu.utils.CipherUtil;

public class BranchService {
	public void register(int id, String branch_name) {

		Connection connection = null;
		try {
			connection = getConnection();

			BranchDao branchDao = new BranchDao();

			branchDao.getBranchList(connection);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}
