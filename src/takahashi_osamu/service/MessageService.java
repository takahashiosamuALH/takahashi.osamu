package takahashi_osamu.service;

import static takahashi_osamu.utils.CloseableUtil.*;
import static takahashi_osamu.utils.DBUtil.*;
import static takahashi_osamu.utils.CloseableUtil.*;
import static takahashi_osamu.utils.DBUtil.*;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import takahashi_osamu.beans.Message;
import takahashi_osamu.beans.User;
import takahashi_osamu.beans.UserMessage;
import takahashi_osamu.dao.MessageDao;
import takahashi_osamu.dao.UserDao;
import takahashi_osamu.dao.UserMessageDao;
import takahashi_osamu.utils.CipherUtil;
import takahashi_osamu.beans.Message;
import takahashi_osamu.dao.MessageDao;

public class MessageService {
	public void register(Message message) {

		Connection connection = null;
		try {
			connection = getConnection();

			MessageDao messageDao = new MessageDao();
			messageDao.insert(connection, message);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}


	private static final int LIMIT_NUM = 1000;

	public List<UserMessage> getMessage() {

		Connection connection = null;
		try {
			connection = getConnection();

			UserMessageDao messageDao = new UserMessageDao();
			List<UserMessage> ret = messageDao.getUserMessages(connection, LIMIT_NUM);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void deleteMessage(int deleteMessage) {

		Connection connection = null;
		try {
			connection = getConnection();

			MessageDao messageDao = new MessageDao();
			messageDao.delete(connection, deleteMessage);

			commit(connection);

		} finally {
			close(connection);
		}
	}

	public List<UserMessage> getSearch(String dateStart, String dateGoal, String category) {
		Connection connection = null;

		try {
			connection = getConnection();

			UserMessageDao userMessageDao = new UserMessageDao();
			List<UserMessage> ret = userMessageDao.getSearch(connection, dateStart, dateGoal, category, LIMIT_NUM);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			 throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}
