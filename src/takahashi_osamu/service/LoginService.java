package takahashi_osamu.service;


import static takahashi_osamu.utils.CloseableUtil.*;
import static takahashi_osamu.utils.DBUtil.*;

import java.sql.Connection;

import takahashi_osamu.beans.User;
import takahashi_osamu.dao.UserDao;
import takahashi_osamu.utils.CipherUtil;




public class LoginService {

	public User login(String login_id, String password) {

		Connection connection = null;
		try {
			connection = getConnection();

			UserDao userDao = new UserDao();
			String encPassword = CipherUtil.encrypt(password);
			User user = userDao.getUser(connection, login_id, encPassword);

			commit(connection);

			return user;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
            rollback(connection);
            throw e;
		} finally {
            close(connection);
        }
	}
}
