package takahashi_osamu.service;

import static takahashi_osamu.utils.CloseableUtil.*;
import static takahashi_osamu.utils.DBUtil.*;

import java.sql.Connection;
import java.sql.SQLException;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.List;

import javax.servlet.http.HttpServlet;

import org.apache.commons.lang.StringUtils;

import takahashi_osamu.dao.BranchDao;
import takahashi_osamu.dao.PositionDao;
import takahashi_osamu.dao.UserDao;
import takahashi_osamu.utils.CipherUtil;
import takahashi_osamu.beans.Branch;
import takahashi_osamu.beans.Position;
import takahashi_osamu.beans.User;

public class UserService {
	public void register(User user) {

		Connection connection = null;
		try {
			connection = getConnection();

			String encPassword = CipherUtil.encrypt(user.getPassword());

			user.setPassword(encPassword);

			UserDao userDao = new UserDao();



			userDao.insert(connection, user);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public User getLoginIdCheck(String login_id) throws SQLException {

		Connection connection = null;

		try {
			connection = getConnection();

			UserDao userDao = new UserDao();

			User ret = userDao.getLoginIdCheck(connection, login_id);

			commit(connection);


			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public User getUser(int id) throws SQLException {

		Connection connection = null;

		try {
			connection = getConnection();

			UserDao userDao = new UserDao();

			User ret = userDao.getUserSettings(connection, id);

			commit(connection);


			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void update(User user) {
		Connection connection = null;

		try {
			connection = getConnection();

			if(!StringUtils.isBlank(user.getPassword())) {
				String encPassword = CipherUtil.encrypt(user.getPassword());
				user.setPassword(encPassword);
			} else {
				String encPassword = user.getPassword();
				user.setPassword(encPassword);
			}
			UserDao userDao = new UserDao();
			userDao.update(connection, user);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			 throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void usersCondition(User user) {
		Connection connection = null;

		try {
			connection = getConnection();

			UserDao userDao = new UserDao();
			userDao.onoff(connection, user);

			commit(connection);


		} catch (RuntimeException e) {
			rollback(connection);
			 throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}



	public List<User> getUsersList() throws SQLException {

		Connection connection = null;

		try {
			connection = getConnection();

			UserDao userDao = new UserDao();

			List<User> ret = userDao.getUsersList(connection);

			commit(connection);


			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public List<Branch> getBranchList() throws SQLException {

		Connection connection = null;

		try {
			connection = getConnection();

			BranchDao branchDao = new BranchDao();

			List<Branch> ret = branchDao.getBranchList(connection);

			commit(connection);


			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public List<Position> getPositionList() throws SQLException {

		Connection connection = null;

		try {
			connection = getConnection();

			PositionDao positionDao = new PositionDao();

			List<Position> ret = positionDao.getPositionList(connection);

			commit(connection);


			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}
