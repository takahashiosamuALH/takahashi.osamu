package takahashi_osamu.service;

import static takahashi_osamu.utils.CloseableUtil.*;
import static takahashi_osamu.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import takahashi_osamu.beans.Comment;
import takahashi_osamu.beans.UserComment;
import takahashi_osamu.beans.UserMessage;
import takahashi_osamu.dao.CommentDao;
import takahashi_osamu.dao.MessageDao;
import takahashi_osamu.dao.UserCommentDao;
import takahashi_osamu.dao.UserMessageDao;

public class CommentService {

	public void register(Comment comment) {

		Connection connection = null;
		try {
			connection = getConnection();

			CommentDao commentDao = new CommentDao();
			commentDao.insert(connection, comment);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}


	private static final int LIMIT_NUM = 1000;

	public List<UserComment> getComment() {

		Connection connection = null;
		try {
			connection = getConnection();

			UserCommentDao commentDao = new UserCommentDao();
			List<UserComment> ret = commentDao.getUserComment(connection, LIMIT_NUM);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void deleteComment(int deleteComment) {

		Connection connection = null;
		try {
			connection = getConnection();

			CommentDao commentDao = new CommentDao();
			commentDao.delete(connection, deleteComment);

			commit(connection);

		} finally {
			close(connection);
		}
	}


}
