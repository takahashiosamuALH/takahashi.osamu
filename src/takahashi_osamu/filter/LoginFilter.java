package takahashi_osamu.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import takahashi_osamu.beans.User;

@WebFilter("/*")
public class LoginFilter implements Filter{

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException,ServletException{

		HttpSession session = ((HttpServletRequest)request).getSession();
		User loginUser = (User) session.getAttribute("loginUser");


		String servletPath = ((HttpServletRequest) request).getServletPath();


		if(servletPath.equals("/login")){
			chain.doFilter(request, response);
			return;
		}

		if(servletPath.matches("^.*.css$")){
			chain.doFilter(request, response);
			return;
		}

		if(servletPath.matches("^.*.js$")){
			chain.doFilter(request, response);
			return;
		}



		if(loginUser == null){
			List<String> messages = new ArrayList<String>();
			messages.add("ログインを行ってください。");
            session.setAttribute("errorMessages", messages);

			// セッションがNullならば、ログイン画面へ飛ばす
			((HttpServletResponse)response).sendRedirect("login");
			return;
		}






		// セッションがNULLでなければ、通常どおりの遷移
		chain.doFilter(request, response);
	}
	@Override
	public void init(FilterConfig config) {}
	@Override
	public void destroy(){}

}