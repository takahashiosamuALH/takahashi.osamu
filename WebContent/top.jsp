<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<script type="text/javascript" src="./js/jquery-3.3.1.min.js"></script>
		<script type="text/javascript" src="./js/takahashi_osamuJs.js"></script>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="./css/style.css" rel="stylesheet" type="text/css">
		<title>社内掲示板</title>

	</head>
	<body>
	<div id="title">社内掲示板</div>
	<div style="position:fixed; width:100%; text-align:right;"><a href="logout" id="logout">ログアウト</a></div>
		<div class="header">
				<a href="./" id="menu">ホーム</a>
				<a href="message" id="menu">新規投稿</a>
				<c:if test="${loginUser.branch_id == 1 && loginUser.position_id == 1}" >
					<a href="usercontrol" id="menu">管理ページ</a>
				</c:if>

		</div>

		<div class="search">
			<div class="box-title">検　　索</div>
			<form action="./">
				カテゴリ検索
				<input type="text" name="category" value="${category}" id="category" placeholder="カテゴリ"><br><br>
				期間検索<br>
				始まり<br><input type="date" name="dateStart" value="${from}" id="dateStart"><br><br>
				終わり<br><input type="date" name="dateGoal" value="${to}" id="dateGoal"><br><br>
				<input type="submit" value="検　索"  id="search"/>
			</form>
		</div>


		<div class="maincontents">
			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session"/>
			</c:if>




			<c:forEach items="${messages}" var="message">
				<div class="messages">
					<div class="message">
						<p><span class="name">投稿者：<c:out value="${message.name}" /></span></p>
						<div class="title">件　名：<c:out value="${message.title}" /></div>
						<div class="category">カテゴリー：<c:out value="${message.category}" /></div>
						<div class="text">本　文：<br></div>
						<c:forEach var="text" items="${fn:split(message.text, '
						')}" >
							<c:out value="${text}" /><br>
						</c:forEach><br>
						<div class="date">作成日時：<fmt:formatDate value="${message.created_at}" pattern="yyyy/MM/dd HH:mm:ss" /></div>


						<div style="text-align:right;">
							<c:if test="${loginUser.id == message.user_id}">
								<form action="delete" method="post" onsubmit="return check('この投稿を削除しますか？')" >
									<input class="Del" type="submit" value="投稿の削除" >
									<input name="message_id" id="message_id" value="${message.id}" type="hidden"/>
								</form>
							</c:if>
						</div>
					</div>

					<c:forEach items="${comments}" var="comment">
						<c:if test="${message.id == comment.message_id}">
							<div class="comments">
								<div class="commentname">コメント者：<c:out value="${comment.name}" /></div>
								<div class="comment">コメント：<br>
								<c:forEach var="text" items="${fn:split(comment.text, '
								')}" >
									<c:out value="${text}" /><br>
								</c:forEach></div><br/>
								<div class="commentdate">コメント日時：<fmt:formatDate value="${comment.created_at}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
								<div  style="text-align:right;">
									<c:if test="${loginUser.id == comment.user_id}">
										<form action="delete" method="post" onsubmit="return check('このコメントを削除しますか？');">
											<input class="Del" type="submit" value="コメントの削除">
											<input name="comment_id" id="comment_id" value="${comment.id}" type="hidden"/>
										</form>
									</c:if>
								</div>
							</div>
						</c:if>
					</c:forEach><br>

						<form action="comment" method="post">
							<input name="user_id" id="user_id" value="${loginUser.login_id}" type="hidden"/>
							<textarea name="text" cols="50" rows="5" class="comment-box"></textarea>
							<input name="message_id" id="message_id" value="${message.id}" type="hidden"/>
							<br />
							<input type="submit" value="コメントの投稿">（500文字まで）
						</form>

					<br />
				</div>
			</c:forEach>
		</div>

		<div class="copyright"> Copyright(c)Takahashi.Osamu</div>

		<script type="text/javascript"><!--
			function check(message) {
				var result = window.confirm(message);
				if( result ) {
					return true;
					//「true」の処理
				} else {
				return false;
				//「false」の処理
				}
			}
		</script>

	</body>
</html>