<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<script type="text/javascript" src="./js/jquery-3.3.1.min.js"></script>
		<script type="text/javascript" src="./js/takahashi_osamuJs.js"></script>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="./css/style.css" rel="stylesheet" type="text/css">
		<title>ユーザー管理</title>
	</head>
	<body>

		<div class="header">
				<a href="./" id="menu">ホーム</a>
				<a href="usercontrol" id="menu">管理ページ</a>
				<a href="signup" id="menu">新規ユーザー作成</a>
		</div>


	<div id="title">ユーザー一覧</div>
	<div style="position:fixed; width:100%; text-align:right;"><a href="logout" id="logout">ログアウト</a></div>
		<div class="maincontents">


			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session"/>
			</c:if>


		<div class="user">

<TABLE border="3" width="100%"  style="border-style:solid ; border-width:1px ;">
	<TR bgcolor="#FFCC99" >
		<TH style="border-style:none;">ログインＩＤ</TH>
		<TH style="border-style:none;">　名　　前　</TH>
		<TH style="border-style:none;"> 支　店　名 </TH>
		<TH style="border-style:none;">部署・役職名</TH>
		<TH style="border-style:none;"> 停止・復活 </TH>
	</TR>
	<c:forEach items="${users}" var="users">
	<c:if test="${users.is_deleted == 1}">
	<div>
	<TR bgcolor="#b5ffbe">
		<TD align="center" style="border-style:none;"><a href="settings?id=${users.id}">${users.login_id}</a></TD>
		<TD align="center" style="border-style:none;">${users.name}</TD>
		<TD align="center" style="border-style:none;">${users.branch_name}</TD>
		<TD align="center" style="border-style:none;">${users.position_name}</TD>
		<TD align="center" style="border-style:none;">
			<c:if test="${users.id == loginUser.id}">
				ログイン中
			</c:if>
			<c:if test="${users.id != loginUser.id}">
				<c:if test="${users.is_deleted == 1}">
					<form action="usercontrol" method="post" onsubmit="return check('このアカウントを停止させてよろしいですか？');">
						<input name="is_deleted" id="is_deleted" value="0" type="hidden"/>
						<input name="id" value="${users.id}" id="id" type="hidden"/>
						<input type="submit" value="停止" />
					</form>
				</c:if>
				<c:if test="${users.is_deleted == 0}">
					<form action="usercontrol" method="post" onsubmit="return check('このアカウントを復活させてよろしいですか？');">
						<input name="is_deleted" id="is_deleted" value="1" type="hidden"/>
						<input type="submit" value="復活" />
						<input name="id" value="${users.id}" id="id" type="hidden"/>
					</form>
				</c:if>
			</c:if>
		</TD>
	</TR>
	</div>
	</c:if>

	<c:if test="${users.is_deleted == 0}">
	<div>
	<TR id="stop" bgcolor="#e7e7e7">
		<TD align="center" style="border-style:none;"><a href="settings?id=${users.id}">${users.login_id}</a></TD>
		<TD align="center" style="border-style:none;">${users.name}</TD>
		<TD align="center" style="border-style:none;">${users.branch_name}</TD>
		<TD align="center" style="border-style:none;">${users.position_name}</TD>
		<TD align="center" style="border-style:none;">
			<c:if test="${users.id == loginUser.id}">
				ログイン中
			</c:if>
			<c:if test="${users.id != loginUser.id}">
				<c:if test="${users.is_deleted == 1}">
					<form action="usercontrol" method="post" onsubmit="return check('このアカウントを停止させてよろしいですか？');">
						<input name="is_deleted" id="is_deleted" value="0" type="hidden"/>
						<input name="id" value="${users.id}" id="id" type="hidden"/>
						<input type="submit" value="停止" />
					</form>
				</c:if>
				<c:if test="${users.is_deleted == 0}">
					<form action="usercontrol" method="post" onsubmit="return check('このアカウントを復活させてよろしいですか？');">
						<input name="is_deleted" id="is_deleted" value="1" type="hidden"/>
						<input type="submit" value="復活" />
						<input name="id" value="${users.id}" id="id" type="hidden"/>
					</form>
				</c:if>
			</c:if>
		</TD>
	</TR>
	</div>
	</c:if>
	</c:forEach>
</TABLE>
		</div>
	</div>
		<div class="copyright"> Copyright(c)Takahashi.Osamu</div>

		<script type="text/javascript"><!--
						function check(message) {
							var result = window.confirm(message);
						if( result ) {
							return true;
							//「true」の処理
						} else {
							return false;
							//「false」の処理
						}
					}
					</script>

	</body>
</html>