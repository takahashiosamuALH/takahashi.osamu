<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<script type="text/javascript" src="./js/jquery-3.3.1.min.js"></script>
		<script type="text/javascript" src="./js/takahashi_osamuJs.js"></script>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="./css/style.css" rel="stylesheet" type="text/css">
		<title>ユーザー新規作成</title>
	</head>
	<body>

	<div id="title">ユーザー新規作成</div>
	<div style="position:fixed; width:100%; text-align:right; top: 0; margin-top:8px;"><a href="logout" id="logout">ログアウト</a></div>

	<div class="header">
		<a href="./" id="menu">ホーム</a>
		<a href="usercontrol" id="menu">管理ページ</a>
		<a href="signup" id="menu">新規ユーザー作成</a>
	</div>


		<div class="main">

			<%-- test 条件指定 --%>
			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session"/>
			</c:if>

			<%-- form データを	action 先に送信する method 使用するメソッド指定、post , get --%>
			<form action="signup" method="post">
			<div class="post">
				<%-- 半角英数[azAZ0*9] で６文字以上２０文字以内 --%>
				<label for="login_id">ログインＩＤ(半角英数、6文字以上20以下で設定してください)</label><br />
				<input name="login_id" value="${user.login_id}" id="login_id" /> <br />

				<%-- 記号も含む半角文字で６文字以上２０文字以内 --%>
				<label for="password">パスワード(半角英数、6文字以上20以下で設定してください)</label><br />
				<input name="password" type="password"  id="password" /><br />
				<label for="passwordcheck">パスワード（確認）</label><br />
				<input name="passwordcheck" type="password"  id="passwordcheck" /><br />

				<%-- １０文字以下 --%>
				<br><label for="name">名前(10文字以下で設定してください)</label> <br />
				<input name="name" value="${user.name}" id="name" /><br /> <br />

				<%-- 支店にリンクするコード --%>
				<label for="branch_id">支店</label><br/>
					<label1>
						<select name="branch_id" id="select">
							<option value=0>選択
							<c:forEach items="${branch}" var="branch">
								<option value="${branch.id}"
									<c:if test="${user.branch_id == branch.id}">
										selected
									</c:if>
								>${branch.branch_name}
							</c:forEach>
						</select><br>
					</label1><br>
				<%-- 部署・役職にリンクするコード --%>
				<label for="position_id">部署・役職</label><br />
					<label1>
						<select name="position_id" id="select"><br>
							<option value=0>選択
							<c:forEach items="${position}" var="position">
								<option value="${position.id}"
								<c:if test="${user.position_id == position.id}">
									selected
								</c:if>
								>${position.position_name}
							</c:forEach>
						</select><br><br>
					</label1>
				</div>
				<br /> <input type="submit" value="登録" id="set"/>

			</form>
			<div class="copyright">Copyright(c)Takahashi.Osamu</div>
		</div>

	</body>
</html>