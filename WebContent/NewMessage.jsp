<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<script type="text/javascript" src="./js/jquery-3.3.1.min.js"></script>
		<script type="text/javascript" src="./js/takahashi_osamuJs.js"></script>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="./css/style.css" rel="stylesheet" type="text/css">
			<title>新規投稿</title>
	</head>
	<body>
		<div id="title">新規投稿</div>
		<div style="position:fixed; width:100%; text-align:right;"><a href="logout" id="logout">ログアウト</a></div>

		<div class="header">
				<a href="./" id="menu">ホーム</a>
				<a href="message" id="menu">新規投稿</a>
		</div>

	<div class="maincontents">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session"/>
		</c:if>




		<div class="form-area">
			<form action="message" method="post" >
				＜件名＞<br />
				<input name="title" value="${message.title}" id="title-box"/><br />
				（30文字まで）
				<br /><br />
				＜カテゴリー＞<br />
				<input name="category" value="${message.category}" id="category-box"/><br /><br />
				（10文字まで）
				＜本文＞<br />
				<textarea name="text"nm cols="50" rows="5" id="text-box">${message.text}</textarea><br />
				（1000文字まで）
				<br /><br /><br />

				<br /><br /><br />
				<input type="submit" value="投稿" id="go"><br /><br />

			</form>
		</div>
		</div>
	</body>
</html>