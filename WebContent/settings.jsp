<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<script type="text/javascript" src="./js/jquery-3.3.1.min.js"></script>
		<script type="text/javascript" src="./js/takahashi_osamuJs.js"></script>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="./css/style.css" rel="stylesheet" type="text/css">
		<title>ユーザー編集</title>
	</head>
	<body>

	<div class="header">
		<a href="./" id="menu">ホーム</a>
		<a href="message" id="menu">新規投稿</a>
		<a href="usercontrol" id="menu">管理ページ</a>
		<a href="signup" id="menu">新規ユーザー作成</a>
	</div>

		<div id="title">ユーザー編集</div>
		<div style="position:fixed; width:100%; text-align:right;"><a href="logout" id="logout">ログアウト</a></div>
		<div class="settings">
			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session"/>
			</c:if>

			<form action="settings" method="post" ><br />
			 <div class="post">
				<input name="id" value="${editUser.id}" id="id" type="hidden"/>
				<label for="login_id">ログインＩＤ(半角英数、6文字以上20以下で設定してください)</label><br />
				<input name="login_id" value="${editUser.login_id}" id="login_id"/><br /><br />

				<label for="password">パスワード(半角英数、6文字以上20以下で設定してください)</label><br />
				<input name="password" type="password" id="password"/> <br />
				<label for="passwordcheck">パスワード(確認)</label><br />
				<input name="passwordcheck" type="password"  id="passwordcheck" /><br /><br />

				<label for="name">名前(10文字以下で設定してください。)</label><br />
				<input name="name" value="${editUser.name}" id="name"/><br /><br />

				<label for="brach_id">支店</label>

				<c:if test="${editUser.id == loginUser.id}">
					<c:forEach items="${branch}" var="branch">
						<c:if test="${editUser.branch_id == branch.id}">
						※自分自身の支店は変更できません
							<br>本店
						</c:if>
					</c:forEach><br>
				</c:if>

				<c:if test="${editUser.id != loginUser.id}">
					<f1>
						<label1>
							<select name="branch_id" id="select"><br>
								<option value=0>選択
								<c:forEach items="${branch}" var="branch">
									<option value="${branch.id}"
									<c:if test="${editUser.branch_id == branch.id}">
										selected
									</c:if>
									>${branch.branch_name}
								</c:forEach>
							</select><br>
						</label1>
					</f1>
				</c:if><br>

				<label for="position_id">部署・役職</label>

				<c:if test="${editUser.id == loginUser.id}">
					<c:forEach items="${position }" var="position">
						<c:if test="${editUser.position_id == position.id}">
						※自分自身の部署・役職は変更できません
							<br>総務部
						</c:if>
					</c:forEach><br>
				</c:if>

				<c:if test="${editUser.id != loginUser.id}">
					<f1>
						<label1>
							<select name="position_id" id="select">
								<option value=0>選択
								<c:forEach items="${position}" var="position">
									<option value="${position.id}"
									<c:if test="${editUser.position_id == position.id}">
										selected
									</c:if>
									>${position.position_name}
								</c:forEach>
							</select><br><br>
						</label1>
					</f1>
				</c:if><br>
				</div>
				<input type="submit" value="登録" id="set"/> <br />

			</form>
		</div>
		<div class="copyright"> Copyright(c)Takahashi.Osamu</div>
	</body>
</html>