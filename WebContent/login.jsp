<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<script type="text/javascript" src="./js/jquery-3.3.1.min.js"></script>
		<script type="text/javascript" src="./js/takahashi_osamuJs.js"></script>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="./css/style.css" rel="stylesheet" type="text/css">
		<title>ログイン</title>
	</head>
	<body>
		<div id="title">社内掲示板</div><br>



		 <div class="login">
		 <div id="login_title">ログイン</div>

		<c:if test="${ not empty errorMessages }">
			<div class="errorMessagesLogin">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session"/>
		</c:if>

		<form action="login" method="post" class="form"><br />

			<p><label for="login_id">ログインＩＤ</label></p>
			<p><input name="login_id" value="${user.login_id}" id="login_id"/>

			<p><label for="password">パスワード</label></p>
			<p><input name="password" type="password" id="password"/></p>

			<p><input type="submit" value="ログイン" id="go" /></p> <br />

      		</form>
      		<div class="copyright"> Copyright(c)Takahashi.Osamu</div>
      	</div>
    </body>
</html>