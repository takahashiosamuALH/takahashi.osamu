

create database takahashi_osamu;

show databases;

use takahashi_osamu;

show tables;

CREATE TABLE users(
id INTEGER PRIMARY KEY AUTO_INCREMENT,
login_id VARCHAR(20) NOT NULL unique,
password VARCHAR(255) NOT NULL,
name VARCHAR(10) NOT NULL,
branch_id INTEGER NOT NULL,
position_id INTEGER NOT NULL,
is_deleted INTEGER NOT NULL
);


CREATE TABLE messages(
id INTEGER PRIMARY KEY AUTO_INCREMENT,
title VARCHAR(30) NOT NULL,
text VARCHAR(1000) NOT NULL,
created_at timestamp NOT NULL,
user_id INTEGER NOT NULL,
category VARCHAR(30) NOT NULL
);


CREATE TABLE comments(
id INTEGER PRIMARY KEY AUTO_INCREMENT,
text VARCHAR(500) NOT NULL,
created_at timestamp NOT NULL,
message_id INTEGER NOT NULL,
user_id INTEGER NOT NULL
);


CREATE TABLE branches(
id INTEGER PRIMARY KEY AUTO_INCREMENT,
name VARCHAR(20) NOT NULL
);


CREATE TABLE positions(
id INTEGER PRIMARY KEY AUTO_INCREMENT,
name VARCHAR(20) NOT NULL
);


